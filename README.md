# spring-boot-data-supporter

QQ交流群：

![alt text](./src/main/resources/static/demo/qq.png "QQ群")

本项目的Mysql和MongoDB数据库（下面是详细的处理过程，免做伸手党，学习学习没坏处）：

https://git.oschina.net/duhongming/spring-boot-data-supporter/attach_files

## [new-使用NLP和Hadoop统计分词个数](https://note.youdao.com/ynoteshare1/index.html?id=e3f6c27be3d2498c1cbbc08f86da0d8f&type=note)
## [new-基金决策分析](https://note.youdao.com/ynoteshare1/index.html?id=1f81ce87cc39ddac56ce4de7a834e7f2&type=note)
## [new-慕课网数据爬取与分析](http://note.youdao.com/noteshare?id=4414aaa64d7ca9805be4a04bec4b200b)
## [1 地理区域5级联动](#1-地理区域5级联动-1)
## [2 身份证信息](#2-身份证信息-1)
## [3 全国天气情况](#3-全国天气情况-1)
## [4 基金决策分析](#4-基金决策分析-1)
## [5 ip地址定位库](#5-ip地址定位库-1)
## [6 一元线性回归获取样本数据建立模型](#6-一元线性回归获取样本数据建立模型-1)
## [7 Ansj中文分词服务](#7-ansj中文分词服务-1)





## 1 地理区域5级联动

### 1.1 数据源导入到Mysql和MongoDB中
-------------------------------------------------------------------------------

1) 下载中国5级行政区域mysql库：https://github.com/kakuilan/china_area_mysql

爬取国家统计局官网的行政区域数据：http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/

2) 通过Navicat，在数据库上右键 >>> 运行SQL文件
```
#数据库设置问题：set global max_allowed_packet=1024*1024*160这样就好了（因为太大了100多M，所以我设置了160M）：

ERROR 2006 (HY000) at line 22: MySQL server has gone away
Operation failed with exitcode 1
```

3） 重命名表：cnarea_2018 >>> cnarea

--------------------------------------------------------------------------------

4）Mysql转MongoDB数据库用的是MongoVUE，Collections右键>>>RDBMS Import>>>from Mysql


5）更新数据库中集合的字段
```
db.cnarea.update({}, {$rename : {"parent_id" : "parentId","area_code" : "areaCode","zip_code" : "zipCode","city_code" : "cityCode","short_name" : "shortName","merger_name" : "mergerName"}}, false, true);
```


6）windows导出MongoDB数据库
```
mongodump -d data-supporter -c cnarea -o D:/mongodump/
```

7）windows导入MongoDB数据库(支持跨服务器，非常方便，速度非常快）
```
mongorestore -h 192.168.1.108 -d data-supporter --dir D:/mongodump/data-supporter/
```
--------------------------------------------------------------------------------

### 1.2 修改数据库配置信息
application.properties配置文件中
```
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/data-supporter
spring.datasource.username=root
spring.datasource.password=root
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.data.mongodb.uri=mongodb://127.0.0.1:27017/data-supporter
```
### 1.3 请通过swagger进行项目调试

http://localhost:8080/swagger-ui.html


地理区域省级调试页面：
![alt text](./src/main/resources/static/demo/province.png "地理区域省级调试页面")

地理区域第2-5级调试页面：
![alt text](./src/main/resources/static/demo/level.png "地理区域第2-5级调试页面")

### 1.4 地理区域5级联动演示页面

http://localhost:8080/

![alt text](./src/main/resources/static/demo/cnarea.png "地理区域5级联动")


## 2 身份证信息

### 2.1 下载身份证信息json库

https://github.com/NoBey/IdCard

下载IdCard/data/data.json，重命名为idcard.json，放到resources\static文件夹下

### 2.2 身份证信息用Gson将Json数据转成Map，其他的Json框架都试了，都不好用
```
Map<String, Object> map = JsonUtil.toMap(JsonUtil.parseJson(injectInfo()));
```

### 2.3 idcard身份证信息获取出生年月日、岁数、性别、生肖、星座、地理位置
![alt text](./src/main/resources/static/demo/idcard.png "idcard身份证")
![alt text](./src/main/resources/static/demo/idcardinfo.png "idcard身份证信息")



## 3 全国天气情况

### 3.1 全国天气情况

http://www.weather.com.cn/

### 3.2 获取某个城市的一段时间的天气
![alt text](./src/main/resources/static/demo/cnweather.png "天气详细信息测试")
![alt text](./src/main/resources/static/demo/cnweather_result.png "天气详细信息结果")

## 5 ip地址定位库

1) 下载ip地址定位库：https://git.oschina.net/lionsoul/ip2region

2) 在application.properties配置关联路径即可：

```
ip.region.db.file=D:\\gitRespo\\open-source\\spring-boot-data-supporter\\resources\\static\\ipdata\\ip2region.db
```
3） 演示地址：http://localhost:8080/swagger-ui.html

![alt text](./src/main/resources/static/demo/ip2region.png "ip地址定位")


## 6 一元线性回归获取样本数据建立模型
![alt text](./src/main/resources/static/demo/monadicline.png "一元线性回归获取样本数据建立模型")

## 7 Ansj中文分词服务
https://github.com/NLPchina/ansj_seg

**Spring Boot 教程大家学习下：**
http://blog.didispace.com/categories/Spring-Boot/page/4/
